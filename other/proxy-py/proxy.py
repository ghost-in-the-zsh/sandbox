import os
import sys
import tty
import pty
import termios
import argparse as ap

from select import select


MAX_BYTES = 256


def main() -> int:
    args = parse_args()

    if args.use_pty_mod:
        fn_impl = ptymod_impl
    else:
        fn_impl = osmod_impl

    fn_impl(args)
    return 0


def ptymod_impl(args: ap.Namespace) -> None:
    pid, master_fd = pty.fork()
    if pid == pty.CHILD:
        ptymod_child(args.command)
    else:
        ptymod_parent(master_fd)


def ptymod_parent(mfd: int) -> None:
    # we want to put the parent's side of the PTY into "raw" mode, which
    # means "input is available character by character, echoing is
    # disabled, and all special processing of terminal input and output
    # characters is disabled." (see `man termios`);
    # mainly, what we want is to disable echoing, so that your inputs
    # don't show up twice in a row
    tty.setraw(mfd, termios.TCSANOW)

    # wait for I/O on the child's PTY and the user's STDIN stream
    fds = [mfd, pty.STDIN_FILENO]
    while True:
        try:
            # select(...) -> (rfds, wfds, xfds):
            #   wait until one or more file descriptors are ready for I/O
            # rfds = file descriptors ready for reading
            # wfds = ditto, for writing
            # xfds = ditto, for exceptional conditions
            rfds, _, _ = select(fds, [], [])
        except KeyboardInterrupt:
            # user has caused an interrupt event (e.g. Ctrl+C)
            break

        if mfd in rfds:
            # the child process has sent something to the parent's PTY
            # and we're ready to read from it to show it in the user's
            # terminal
            try:
                data = os.read(mfd, MAX_BYTES)
                if not data:
                    break   # got en EOF (e.g. Ctrl+D)
                os.write(pty.STDOUT_FILENO, data)
            except OSError:
                # a read/write error might be caused by the child process
                # closing the other end (e.g. exiting, crashing, etc.)
                break

        if pty.STDIN_FILENO in rfds:
            # the user entered something in its terminal that should be
            # sent to the child process via its PTY file descriptor
            try:
                data = os.read(pty.STDIN_FILENO, MAX_BYTES)
                if not data:
                    break
                os.write(mfd, data)
            except OSError:
                break

    os.close(mfd)


def ptymod_child(command: str) -> None:
    argv = command.split()
    os.execvp(argv[0], argv)
    raise RuntimeError(f'os.execvp: {argv}')


def osmod_impl(args: ap.Namespace) -> None:
    master_fd, slave_fd = os.openpty()
    pid = os.fork()
    if pid == 0:
        osmod_child(args.command, master_fd, slave_fd)
    else:
        osmod_parent(master_fd, slave_fd)


def osmod_parent(mfd: int, sfd: int):
    # slave FD is invalid in parent
    os.close(sfd)
    ptymod_parent(mfd)


def osmod_child(command: str, mfd: int, sfd: int):
    # master FD is invalid in child
    os.close(mfd)

    # processes are organized into process groups; process groups are
    # organized into sessions; sessions may (not) have a controlling
    # terminal (ctty); since this process is someone else's child, and
    # needs its own ctty, it needs to have its own session
    os.setsid()

    # we want to put the child's ctty into "raw" mode, which means "input
    # is available character by character, echoing is disabled, and all
    # special processing of terminal input and output characters is
    # disabled." (see `man termios`)
    #
    # see:
    #   https://docs.python.org/3/library/termios.html
    #   https://docs.python.org/3/library/tty.html
    tty.setraw(sfd, termios.TCSANOW)

    # create duplicate file descriptors for stdin/stdout/stderr streams
    os.dup2(sfd, sys.stdin.fileno())    # or `os.dup2(sfd, pty.STDIN_FILENO)`
    os.dup2(sfd, sys.stdout.fileno())   # or `os.dup2(sfd, pty.STDOUT_FILENO)`
    os.dup2(sfd, sys.stderr.fileno())   # or `os.dup2(sfd, pty.STDERR_FILENO)`

    os.close(sfd)   # sfd is now unecessary

    ptymod_child(command)


def parse_args() -> ap.Namespace:
    name = os.path.basename(sys.argv[0])

    parser = ap.ArgumentParser(
        description='Demo program showing PTY usage for IPC',
        formatter_class=ap.RawDescriptionHelpFormatter,
        epilog='Example commands: \n' +
              f'  $ python3 {name} -c "python3 upper.py"\n' +
              f'  $ python3 {name} -c bash -b'
    )
    parser.add_argument(
        '-c',
        '--command',
        dest='command',
        metavar='NAME',
        type=str,
        required=True,
        help='name of command to execute as a child process (must be in your $PATH)'
    )
    parser.add_argument(
        '-b',
        '--built-in-mod',
        dest='use_pty_mod',
        action='store_true',
        help='use a `pty` module-based impl. instead of the `os`-based one'
    )
    return parser.parse_args()


if __name__ == '__main__':
    sys.exit(main())
