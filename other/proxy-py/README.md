# Proxy

A Python-based implementation of the C-based [proxy](../proxy) demo program.


## Requirements

- A GNU+Linux system
- Python 3.7.5 (or later)
- A terminal/shell


## Running

This example launches Python3 as the child:

```bash
$ python3 proxy.py -c python3
Python 3.8.2 (default, Apr 27 2020, 15:53:34)
[GCC 9.3.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> print('Hello, World!')
Hello, World!
>>>     # Ctrl+D to send EOF
$
```

To include arguments for the child program, quote the `-c`ommand argument:

```bash
$ python3 proxy.py -c 'python3 -q'  # note -q is for python3 to skip the header
>>> quit()                          # child exits; parent can't read from the PTY master
$
```

Can be any interactive program that relies on a pseudo-terminal:

```bash
$ python3 proxy.py -c bc
bc 1.07.1
Copyright 1991-1994, 1997, 1998, 2000, 2004, 2006, 2008, 2012-2017 Free Software Foundation, Inc.
This is free software with ABSOLUTELY NO WARRANTY.
For details type `warranty'.
5+5
10
6-5
1       # Ctrl+D to send EOF
$
```

There're two different implementations. Look at the built-in options in the help documentation:

```bash
$ python3 proxy.py -h
usage: proxy.py [-h] -c NAME [-b]

Demo program showing PTY usage for IPC

optional arguments:
  -h, --help            show this help message and exit
  -c NAME, --command NAME
                        name of command to execute as a child process (must be in your $PATH)
  -b, --built-in-mod    use a `pty` module-based impl. instead of the `os`-based one

Example commands:
  $ python3 proxy.py -c "python3 upper.py"
  $ python3 proxy.py -c bash -b
```
