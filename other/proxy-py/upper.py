import os
import sys
import argparse as ap


def main() -> int:
    parse_args()
    name = os.path.basename(sys.argv[0])
    while True:
        try:
            data = input(f'{name} >> ')
            if data:
                print(data.upper())
        except EOFError:
            break

    return 0


def parse_args() -> ap.Namespace:
    parser = ap.ArgumentParser(
        description='Trivial program to be used for IPC with `proxy.py`',
        epilog='See the accompanying master program'
    )
    return parser.parse_args()


if __name__ == '__main__':
    sys.exit(main())
