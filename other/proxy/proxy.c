/**
 * required feature-test macros (FTM);
 * see `man posix_openpt`, https://stackoverflow.com/a/5724485/4594973
 **/
#define _XOPEN_SOURCE   700     /* for pty calls */
#define _DEFAULT_SOURCE         /* for cfmakeraw */

#include <stdlib.h>
#include <fcntl.h>              /* file access modes (e.g. O_RDWR) */
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <termios.h>
#include <sys/select.h>
#include <sys/ioctl.h>


const size_t MAX_BUFFER_SIZE = 32;


typedef struct {
    size_t argc;
    char** argv;
} cli_t;


void parent(int fdm, int fds, char* buffer);
void child(int fdm, int fds, char* buffer, cli_t* proc_args);


int main(int argc, char** argv)
{
    int fdm, fds;                   /* file descriptors for master and slave */
    int retval;                     /* store return values (status codes) from some functions */
    char buffer[MAX_BUFFER_SIZE];   /* I/O buffer */
    char* master_path = NULL;       /* full path to master pty */
    cli_t proc_args = {
        .argc = argc,
        .argv = argv
    };

    /* we expect the child program command in `argv[1]` */
    if (argc < 2) {
        fprintf(stderr, "Usage: %s <interactive-command>\n", argv[0]);
        return EXIT_FAILURE;
    }

    /* disable I/O buffering for output */
    setbuf(stdout, NULL);
    setbuf(stderr, NULL);

    /**
     * the parent program (this) sets up both master/slave ptys; it uses
     * the `posix_openpt`, `grantpt`, and `unlockpt` syscalls for the
     * master side, and `open` for the slave side
     **/

    /* open the master pty for reading/writing; see `man posix_openpt` */
    fdm = posix_openpt(O_RDWR);
    if (fdm == -1) {
        perror("posix_openpt");
        return EXIT_FAILURE;
    }
    /* grant access to the slave pty; see `man grantpt` */
    retval = grantpt(fdm);
    if (retval != 0) {
        perror("grantpt");
        return EXIT_FAILURE;
    }
    /**
     * release an internal lock, so that the slave can be opened;
     * it must be called *after* `grantpt`.
     **/
    retval = unlockpt(fdm);
    if (retval != 0) {
        perror("unlockpt");
        return EXIT_FAILURE;
    }

    /**
     * get the path/name of the master, so we can open the slave;
     * note that the pointer is only valid until the next time
     * `ptsname` is called.
     **/
    master_path = ptsname(fdm);
    if (master_path == NULL) {
        perror("ptsname");
        return EXIT_FAILURE;
    }
    /* open the slave side */
    fds = open(master_path, O_RDWR);
    if (fds == -1) {
        perror("open");
        return EXIT_FAILURE;
    }

    switch (fork()) {
        case -1:
            perror("fork");
            return EXIT_FAILURE;
        case 0:
            child(fdm, fds, buffer, &proc_args);
            break;
        default:
            parent(fdm, fds, buffer);
            break;
    }

    return EXIT_SUCCESS;
}


void parent(int fdm, int fds, char* buffer)
{
    /* close the slave side of the pty */
    if (close(fds) != 0) {
        perror("parent: close slave");
        exit(EXIT_FAILURE);
    }

    int eof_received = 0;   /* flag to mark if we've read EOF (e.g. Ctrl+D) */
    int retval;             /* return values from system calls, etc. */
    fd_set fdin_set;        /* see `man fd_set` */
    while (!eof_received) {
        /**
         * the file descriptors set must be reinitialized on every
         * iteration because the `select()` call modifies the
         * structure on every call
         **/
        FD_ZERO(&fdin_set);                 /* zero out the structure */
        FD_SET(STDIN_FILENO, &fdin_set);    /* mark stdin for monitoring */
        FD_SET(fdm, &fdin_set);             /* mark master pty for monitoring */

        /**
         * note that `select(nfds, ...)` will monitor up to `nfds - 1`,
         * file descriptors, so in order to make sure `fdm` is included,
         * we send `fdm + 1`; `select()` tells us how many file descriptors
         * are in a "ready" state and also which ones those are --by
         * modifying the `fd_set` argument; see `man select`;
         *
         * "The ndfs argument must be set one greater than the highest
         * file descriptor number included in any of the three file
         * descriptor sets. This argument allows select() to be more
         * efficient, since the kernel then knows not to check whether
         * file descriptor numbers higher than this value are part of each
         * file descriptor set." (The Linux Programming Interface, p.1332)
         *
         **/
        retval = select(fdm + 1, &fdin_set, NULL, NULL, NULL);
        if (retval == -1) {
            perror("select");
            exit(EXIT_FAILURE);
        }

        /**
         * check if we have data waiting to be read from stdin; this
         * basically asks if the `select()` call has added stdin to the
         * set of file descriptors (i.e. `fd_set`) that's ready for I/O
         **/
        if (FD_ISSET(STDIN_FILENO, &fdin_set)) {
            /* read no more than buffer's capacity */
            retval = read(STDIN_FILENO, buffer, MAX_BUFFER_SIZE);
            switch (retval) {
                case -1:
                    perror("read(stdin)");
                    exit(EXIT_FAILURE);
                case 0:
                    /* got EOF, so we bail */
                    eof_received = 1;
                    break;
                default:
                    /**
                     * got user data from stdin, so we send it to the
                     * child by writing it to the master pty;
                     * `retval` has the number of bytes read before the
                     * `write()` call
                     **/
                    retval = write(fdm, buffer, retval);
                    if (retval == -1) {
                        perror("write(fdm)");
                        exit(EXIT_FAILURE);
                    }
                    break;
            }
        }

        /**
         * check if we have data waiting to be read from the master pty;
         * basically asks if the `select()` call has added the master pty
         * to the set of file descriptors (i.e. `fd_set`) that's ready
         * for I/O; this is output written by the child process
         **/
        if (FD_ISSET(fdm, &fdin_set)) {
            retval = read(fdm, buffer, MAX_BUFFER_SIZE);
            switch (retval) {
                case -1:
                    perror("read(fdm)");    /* child could've exited */
                    exit(EXIT_FAILURE);
                case 0:
                    eof_received = 1;
                    break;
                default:
                    /* got data from child, so we show it to the user on stdout */
                    retval = write(STDOUT_FILENO, buffer, retval);
                    if (retval == -1) {
                        perror("write(stdout)");
                        exit(EXIT_FAILURE);
                    }
                    break;
            }
        }
    }
}


void child(int fdm, int fds, char* buffer, cli_t* proc_args)
{
    struct termios term_settings;   /* terminal settings structure */

    /* close the master side of the pty */
    if (close(fdm) != 0) {
        perror("child: close master");
        exit(EXIT_FAILURE);
    }

    /**
     * get the current state of the slave terminal from the driver
     * to guarantee we have a fully initialized structure
     **/
    tcgetattr(fds, &term_settings);

    /**
     * set raw mode on the slave side; this makes input available
     * character by character, disables echoing, and disables special
     * processing of special input/output characters (e.g. escape
     * sequences for coloring, bold/italics, etc)
     **/
    cfmakeraw(&term_settings);
    tcsetattr(fds, TCSANOW, &term_settings);    /* TCSANOW = make changes immediately */

    /**
     * to bind the child's stdin/stdout/stderr streams to the
     * slave pty, we need to close them first, because they're
     * assigned to the parent's terminal by default; note that `fork`
     * creates an exact copy of the parent, which is tied to the
     * user's terminal
     **/
    if (close(STDIN_FILENO) != 0) {
        perror("child: close(STDIN)");
        exit(EXIT_FAILURE);
    }
    if (close(STDOUT_FILENO) != 0) {
        perror("child: close(STDOUT)");
        exit(EXIT_FAILURE);
    }
    if (close(STDERR_FILENO) != 0) {
        perror("child: close(STDERR)");
        exit(EXIT_FAILURE);
    }

    /**
     * now the child process has nowhere to get stdin from or send
     * stdout/stderr to; we want to bind them to the slave pty, so we
     * duplicate the slave pty file descriptor for each stream.
     * see `man dup`
     **/
    if (dup(fds) == -1) {       /* stdin comes from pty */
        perror("dup(stdin)");
        exit(EXIT_FAILURE);
    }
    if (dup(fds) == -1) {       /* stdout goes to pty */
        perror("dup(stdout)");
        exit(EXIT_FAILURE);
    }
    if (dup(fds) == -1) {       /* stderr goes to pty */
        perror("dup(stderr)");
        exit(EXIT_FAILURE);
    }

    /**
     * at this point, the original file descriptor can be used
     * interchangeably with stdin/stdout/stderr, b/c they're
     * duplicates; the original is now redundant can can be closed
     **/
    if (close(fds) != 0) {
        perror("child: close(fds)");
        exit(EXIT_FAILURE);
    }

    /**
     * processes are grouped together into process groups; process
     * groups are grouped together into sessions; sessions may or
     * may not have controlling terminals (cttys); cttys control processes
     * by sending signals, inputs from stdin, and so on (the kernel knows
     * where to deliver these b/c the cttys are attached to foreground
     * process groups);
     *
     * while processes may receive signals from other *processes* running
     * with other cttys, it will never receive a signal from a tty that is
     * *not* its ctty.
     *
     * at this point, the child is part of the parent's process group,
     * but not the group or session leader; we make the child a session
     * leader so that it can get a controlling tty;
     *
     * see:
     *  DESCRIPTION and NOTES sections of the `man 2 setsid` page,
     *  https://www.informit.com/articles/article.aspx?p=397655&seqNum=6
     **/
    if (setsid() == -1) {
        perror("setsid");
        exit(EXIT_FAILURE);
    }

    /**
     * set the controlling terminal for the child's stdin stream; "The
     * calling process must be a session leader and not have a controlling
     * terminal already."[1]
     *
     * this call *usually* returns 0 on success and *usually* returns -1
     * on error... usually[2]...
     *
     * [1] https://linux.die.net/man/4/tty_ioctl
     * [2] `man 2 ioctl`
     **/
    ioctl(STDIN_FILENO, TIOCSCTTY); /* TIOCSCTTY ?= Tty I/O Control Set Controlling TTY? */

    /**
     * after all the above setup, we now get ready to actually exec
     * the program the parent received on `argv[1]`
     **/
    /* make space for the child's CLI args */
    char** child_argv = (char **)malloc(proc_args->argc * sizeof(char*));
    if (child_argv == NULL) {
        perror("malloc");
        exit(EXIT_FAILURE);
    }

    /**
     * duplicate the CLI args we received, skipping the parent's process
     * name/path in `argv[0]`
     **/
    size_t i;
    for (i = 1; i < proc_args->argc; ++i)
        child_argv[i-1] = strdup(proc_args->argv[i]);

    child_argv[i-1] = NULL; /* yup */

    /**
     * replace this process with the actual child the parent intended
     * to invoke with the original CLI args
     **/
    execvp(child_argv[0], child_argv);

    /**
     * should never get here, because `execvp` does not return; if we're
     * here, something went wrong...
     **/
    perror("execvp");
    exit(EXIT_FAILURE);
}
