# Proxy

A systems programming demo that uses pseudo-terminals (PTYs) as the IPC method between two *interactive* programs. The parent process sets up a master/slave PTY pair and creates a child. The parent expects the name of an arbitrary interactive program on the CLI (e.g. `python3`, `bash`, `bc`, etc.) and forwards it to the child, along with any other arguments. The child sets up the slave side of the PTY, connecting its input/output/error streams to it, and sets up the CLI args to be forwarded. Lastly, it uses `execvp` to replace itself with the requested interactive program.


## Diagram

The basic design is:

```text
                    +-------+
                    |       |
           +--[w]-->| PTY.M |--[r]-->+
          /         |       |         \
+--------+          +--+ +--+          +-------+
| Parent |             |-|             | Child |
+--------+          +--+ +--+          +-------+
          \         |       |         /
           +<--[r]--| PTY.S |<--[w]--+
                    |       |
                    +-------+
```

**Parent**

- Expects arbitrary interactive command name.
- Wait for user input from stdin.
- Send user's input to child by writing it to the stdout/write-end of master PTY.
- Wait for child's output by reading on the stdin/read-end of slave PTY. (This is a blocking read.)
- Write input from slave PTY to stdout for the user to see.


**Child**

- Sets up slave PTY.
- Connects stdin, stdout, and stderr to the PTY.
- Replaces itself with arbitrary interactive command using `execvp`.


## Requirements

- A GNU+Linux system
- The `gcc` and `make` programs
- A terminal/shell


## Building and Running

Use `make`:

```bash
$ make
gcc -O3 --std=c18 -Wpedantic -o proxy proxy.c
```

This example launches Python3 as the child:

```bash
$ ./proxy python3
Python 3.7.5 (default, Nov 20 2019, 09:21:52)
[GCC 9.2.1 20191008] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> print('Hello, World!')
Hello, World!
>>> # press Ctrl+D to send EOF
$
```

Additional arguments also get forwarded to the child:

```bash
$ ./proxy python3 -q    # note -q is for python3 to skip the header
>>> quit()              # child exits; parent can't read from the PTY master
read(fdm): Input/output error
```

Can be any interactive program:

```bash
$ ./proxy bc
bc 1.07.1
Copyright 1991-1994, 1997, 1998, 2000, 2004, 2006, 2008, 2012-2017 Free Software Foundation, Inc.
This is free software with ABSOLUTELY NO WARRANTY.
For details type `warranty'.
4+5
9
10-9
1   # press Ctrl+D
$
```

This example launches the `bash` shell from within my regular `zsh` shell:

```bash
$ echo $$
8942
$ ./proxy bash  # launch bash shell as child
$ echo $$
10395
$ exit          # exit bash; parent in zsh can't read from PTY master
exit
read(fdm): Input/output error
```

To remove the generated file(s), run

```bash
$ make clean
rm proxy
```
