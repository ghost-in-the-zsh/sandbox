#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>


const size_t BUFFER_SIZE = 8;   /* small buffer to break up larger messages :) */


void parent(int* ppfds, int* cpfds, char* buffer);
void child(int* ppfds, int* cpfds, char* buffer);


int main(int argc, char** argv)
{
    int ppfds[2];               /* parent pipe file descriptors */
    int cpfds[2];               /* child pipe file descriptors */
    int bytes_read = 0;
    char buffer[BUFFER_SIZE];

    setbuf(stdout, NULL);       /* disable buffered I/O for output */

    if (pipe(ppfds) != 0) {
        perror("parent pipe");
        return EXIT_FAILURE;
    }
    if (pipe(cpfds) != 0) {
        perror("child pipe");
        return EXIT_FAILURE;
    }

    printf(">> %s <<\n", "Press Ctrl+D to exit (sends EOF)");
    switch (fork()) {
        case -1:
            perror("fork");
            return EXIT_FAILURE;
        case 0:
            child(ppfds, cpfds, buffer);
            break;
        default:
            parent(ppfds, cpfds, buffer);
            break;
    }

    return EXIT_SUCCESS;
}


void parent(int* ppfds, int* cpfds, char* buffer)
{
    /**
     * parent only writes to its own pipe on stdout; stdin is
     * unused and must be closed.
     **/
    if (close(ppfds[STDIN_FILENO]) != 0) {
        perror("close ppfd[STDIN_FILENO]");
        exit(EXIT_FAILURE);
    }
    /**
     * parent only reads from the child's pipe; stdout is unused
     * and must be closed
     **/
    if (close(cpfds[STDOUT_FILENO]) != 0) {
        perror("close cpfd[STDOUT_FILENO]");
        exit(EXIT_FAILURE);
    }

    /**
     * For user-feedback reasons, we want to track whether we've
     * been forced to handle a message by breaking it down into
     * multiple segments, requiring multiple reads/writes, just
     * to make it fit in the buffer.
     **/
    int newmsg = 1;
    while (1) {
        if (newmsg)
            printf("%s ", ">>");

        if (fgets(buffer, BUFFER_SIZE, stdin) == NULL) {
            /**
             * encountered EOF (e.g. user pressed Ctrl+D); try to close remaining
             * pipes on a best-effort basis; if we fail, the OS will clean up anyway
             * so we don't bother to check in this case.
             **/
            fprintf(stderr, "\n%s\n", "Parent cleaning up...");
            close(ppfds[STDOUT_FILENO]);
            close(cpfds[STDIN_FILENO]);
            break;
        }
        int bytes_read = strlen(buffer);

        if (write(ppfds[STDOUT_FILENO], buffer, bytes_read) != bytes_read) {
            perror("write partial/failed on ppfds[STDOUT_FILENO]");
            exit(EXIT_FAILURE);
        }
        /* we read from child; blocks until child writes to pipe */
        bytes_read = read(cpfds[STDIN_FILENO], buffer, BUFFER_SIZE);
        if (bytes_read == -1) {
            perror("read cpfds[STDOUT_FILENO]");
            exit(EXIT_FAILURE);
        }
        if (bytes_read == 0) {
            /* encountered EOF; child closed pipe */
            fprintf(stderr, "\n%s\n", "Parent cleaning up...");
            close(ppfds[STDOUT_FILENO]);
            close(cpfds[STDIN_FILENO]);
            break;
        }

        #ifdef __ECHOPREFIX__
        /* show explicit prefix to mark response */
        if (newmsg)
            printf("%s %s", "<<", buffer);
        else
        #endif
            printf("%s", buffer);

        /**
         * reading a newline means this is the last segment of the
         * current message and whatever we get next will be a new one
         **/
        newmsg = buffer[bytes_read - 1] == '\n' ? 1 : 0;
    }
}


void child(int* ppfds, int* cpfds, char* buffer)
{
    /**
     * child only reads from parent's pipe; stdout is unused and
     * must be closed
     **/
    if (close(ppfds[STDOUT_FILENO]) != 0) {
        perror("close ppfds[STDOUT_FILENO]");
        exit(EXIT_FAILURE);
    }
    /**
     * child only writes to its own pipe on stdout; stdin is unused
     * and must be closed
     **/
    if (close(cpfds[STDIN_FILENO]) != 0) {
        perror("close cpfds[STDIN_FILENO]");
        exit(EXIT_FAILURE);
    }
    /* keep working until we get EOF from parent (i.e. parent closes pipe) */
    while (1) {
        int bytes_read = read(ppfds[STDIN_FILENO], buffer, BUFFER_SIZE);
        if (bytes_read == 0) {
            /**
             * received EOF from parent (e.g. pipe closed), so
             * we're done; try to close remaining pipes on a
             * best-effort basis; if we fail, the OS will clean
             * up anyway so we don't bother to check in this case.
             **/
            fprintf(stderr, "%s\n", "Child cleaning up...");
            close(ppfds[STDIN_FILENO]);
            close(cpfds[STDOUT_FILENO]);
            break;
        }
        if (bytes_read == -1) {
            perror("read ppfds[STDIN_FILENO]");
            exit(EXIT_FAILURE);
        }

        /* just convert to upper case when alphabetic and in lowercase */
        for (size_t i = 0; i < bytes_read; ++i)
            if (isalpha(buffer[i]) && islower(buffer[i]))
                buffer[i] = toupper(buffer[i]);

        /* send back to parent */
        if (write(cpfds[STDOUT_FILENO], buffer, bytes_read) != bytes_read) {
            perror("write partial/failed on cpfds[STDOUT_FILENO]");
            exit(EXIT_FAILURE);
        }
    }
}
