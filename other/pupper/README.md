# P-Upper

A systems programming demo that uses pipes as the IPC method. The process sets itself up as a parent, sets up two pipes, and creates a child. The parent gets the user's input from STDIN, sends it to the child for processing, and then receives the child's output, which the parent then writes to STDOUT.


## Diagram

The basic design is:

```text
                         +--------+
           +--[writes]-->| Pipe 1 |--[reads]-->+
          /              +--------+             \
+--------+                                       +-------+
| Parent |                                       | Child |
+--------+                                       +-------+
          \             +--------+              /
           +<--[reads]--| Pipe 2 |<--[writes]--+
                        +--------+
```

**Parent**

- Wait for user input from stdin.
- Send user's input to child by writing it to the stdout/write-end of Pipe 1.
- Wait for child's output by reading on the stdin/read-end of Pipe 2. (This is a blocking read.)
- Write input from Pipe 2 to stdout for the user to see.


**Child**

- Wait for parent's output from stdin, which is connected to Pipe 1. (This is a blocking read.)
- Convert input to upper case.
- Write converted input to stdout, which is connected to Pipe 2.


## Requirements

You need to have the following:

- A GNU+Linux system
- The `gcc` and `make` programs.
- A terminal


## Building and Running

Use `make` and then launch the program:

```bash
$ make
$ gcc -O3 --std=c18 -o pupper -D__ECHOPREFIX__ pupper.c
$ ./pupper
>> Press Ctrl+D to exit (sends EOF) <<
>> Hello, world!
<< HELLO, WORLD!
>>      # Ctrl+D pressed here
Parent cleaning up...
Child cleaning up...
```

To build the version *without* the `<<` prefix, use `make noprefix`:

```bash
$ make noprefix
gcc -O3 --std=c18 -o pupper pupper.c
$ ./pupper
>> Press Ctrl+D to exit (sends EOF) <<
>> Hello, world!
HELLO, WORLD!
>>      # Ctrl+D pressed here
Parent cleaning up...
Child cleaning up...
```

To remove the generated file(s), run `make clean`.
