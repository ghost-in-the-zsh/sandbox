//
use std::io;
use std::io::prelude::*;


fn main() -> std::io::Result<()> {
    let mut line = String::new();
    let mut bytes: usize;
    loop {
        print!("{}", ">> ");
        io::stdout().flush()?;

        bytes = io::stdin().read_line(&mut line)?;
        if bytes == 0 {
            break;  // got EOF
        }

        print!("{}", line.to_uppercase());
        line.clear();
    }
    Ok(())
}
