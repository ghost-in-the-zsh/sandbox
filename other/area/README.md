# Area

A simple program to calculate the area and include an automated test case.


## Requirements

You need to have the following:

- Linux shell (or equivalent)
- The Rust tool chain (i.e. `cargo`, `rustc`, etc.)


## Building and Running

Just use `cargo run` to build and run automatically:

```bash
$ cargo run -- 3 4
    Finished dev [unoptimized + debuginfo] target(s) in 0.01s
     Running `target/debug/area 3 4`
12
```

To run automated test cases, use `cargo test`

```bash
$ cargo test
    Finished test [unoptimized + debuginfo] target(s) in 0.01s
     Running target/debug/deps/area-417b00b0ebb2a5c9

running 1 test
test test::test_area_matches ... ok

test result: ok. 1 passed; 0 failed; 0 ignored; 0 measured; 0 filtered out
```
