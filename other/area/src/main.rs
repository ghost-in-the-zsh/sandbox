use std::env;
use std::process;
use std::str::FromStr;


fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() != 3 {
        println!("Usage: {name} <width> <height>", name=args[0]);
        process::exit(1);
    }
    let msg = "Values must be real numbers";
    let width = f64::from_str(&args[1]).expect(msg);
    let height = f64::from_str(&args[2]).expect(msg);
    println!("{area}", area=calc_area(width, height));
}


fn calc_area(w: f64, h: f64) -> f64 {
    w * h
}


#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_area_matches() {
        let w: f64 = 5.0;
        let h: f64 = 2.0;
        assert_eq!(w * h, calc_area(w, h));
    }
}
