#ifndef __LINKEDLIST_H__
#define __LINKEDLIST_H__

#include <cassert>
#include <cstddef>
#include <algorithm>
#include <optional>

#include "list.hpp"

template <typename T>
class LinkedList : virtual public List<T>
{
public:
    LinkedList();
    LinkedList(LinkedList<T> &&source);
    LinkedList(const LinkedList<T> &source);
    virtual ~LinkedList();

    virtual void push_front(const T &item);
    virtual void push_back(const T &item);
    virtual std::optional<T> pop_front();
    virtual std::optional<T> pop_back();
    virtual std::optional<std::size_t> has(const T &item) const;
    virtual std::optional<T> get(std::size_t index) const;
    virtual std::size_t len() const;

    bool operator==(const LinkedList<T> &rhs) const;
    bool operator!=(const LinkedList<T> &rhs) const;

    LinkedList<T> &operator=(const LinkedList<T> &rhs);
    LinkedList<T> &operator=(LinkedList<T> &&rhs);

private:
    template <typename R>
    class Node
    {
    public:
        // typedef T value_type;
        // typedef T &ref_type;
        // typedef T const &const_ref_type;
        // typedef T *ptr_type;
        // typedef T const *const_ptr_type;

        Node(const R &value) : value(value),
                               prev(nullptr),
                               next(nullptr)
        {
        }
        R value;
        Node *prev;
        Node *next;
    };

    Node<T> *_head;
    std::size_t _len;

    void copy(const LinkedList<T> &src);
    void reset(LinkedList<T> &list);
    void destroy(LinkedList<T> &list);

    std::optional<Node<T>> find(std::size_t idx) const;
    std::optional<std::size_t> find(const T &item) const;
};

template <typename T>
LinkedList<T>::LinkedList() : _head(nullptr),
                              _len(0)
{
}

template <typename T>
LinkedList<T>::LinkedList(const LinkedList<T> &source) : _head(nullptr),
                                                         _len(0)
{
    copy(source);
}

template <typename T>
LinkedList<T>::LinkedList(LinkedList<T> &&source) : _head(nullptr),
                                                    _len(0)
{
    _head = source._head;
    _len = source._len;

    reset(source);
}

template <typename T>
LinkedList<T>::~LinkedList()
{
    destroy(*this);
}

template <typename T>
void LinkedList<T>::push_front(const T &item)
{
    if (_len == 0)
    {
        _head = new Node<T>(item);
    }
    else
    {
        Node<T> *front = new Node<T>(item);
        front->next = _head;
        _head->prev = front;
        _head = front;
    }
    ++_len;
}

template <typename T>
void LinkedList<T>::push_back(const T &item)
{
    if (_len == 0)
    {
        _head = new Node<T>(item);
    }
    else
    {
        Node<T> *tmp = _head;
        while (tmp->next)
            tmp = tmp->next;

        tmp->next = new Node<T>(item);
        tmp->next->prev = tmp;
    }
    ++_len;
}

template <typename T>
std::optional<T> LinkedList<T>::pop_front()
{
    if (_len == 0)
        return std::nullopt;

    Node<T> *tmp = _head;
    _head = _head->next;
    if (_head)
        _head->prev = nullptr;

    T item = tmp->value;
    delete tmp;
    --_len;

    return std::make_optional<T>(item);
}

template <typename T>
std::optional<T> LinkedList<T>::pop_back()
{
    if (_len == 0)
        return std::nullopt;

    Node<T> *tmp = _head;
    while (tmp->next)
        tmp = tmp->next;

    if (tmp->prev)
        tmp->prev->next = nullptr;

    T item = tmp->value;
    delete tmp;
    --_len;

    return std::make_optional<T>(item);
}

template <typename T>
std::optional<std::size_t> LinkedList<T>::has(const T &item) const
{
    std::size_t idx = 0;
    Node<T> *tmp = _head;
    while (tmp)
    {
        if (tmp->value == item)
            return std::make_optional<std::size_t>(idx);

        tmp = tmp->next;
        ++idx;
    }
    return std::nullopt;
}

template <typename T>
std::optional<T> LinkedList<T>::get(std::size_t index) const
{
    if (index >= _len)
        return std::nullopt;

    std::size_t i = 0;
    Node<T> *tmp = _head;
    while (i++ != index)
        tmp = tmp->next;

    return std::make_optional<T>(tmp->value);
}

template <typename T>
std::size_t LinkedList<T>::len() const
{
    return _len;
}

template <typename T>
bool LinkedList<T>::operator==(const LinkedList<T> &rhs) const
{
    if (_head == rhs._head)
        return true;

    if (_len != rhs._len)
        return false;

    Node<T> *l = _head;
    Node<T> *r = rhs._head;
    while (l && r)
    {
        if (l->value != r->value)
            return false;

        l = l->next;
        r = r->next;
    }
    return true;
}

template <typename T>
bool LinkedList<T>::operator!=(const LinkedList<T> &rhs) const
{
    return !(*this == rhs);
}

template <typename T>
LinkedList<T> &LinkedList<T>::operator=(const LinkedList<T> &rhs)
{
    destroy(*this);
    copy(rhs);
    return *this;
}

template <typename T>
LinkedList<T> &LinkedList<T>::operator=(LinkedList<T> &&rhs)
{
    destroy(*this);
    copy(rhs);
    reset(rhs);
    return *this;
}

template <typename T>
void LinkedList<T>::copy(const LinkedList<T> &src)
{
    if (src._len == 0)
        return;

    Node<T> *src_ptr = src._head;
    Node<T> *dst_ptr = new Node<T>(src_ptr->value);

    _head = dst_ptr;
    _len = src._len;

    for (std::size_t i = 1; i < _len; ++i)
    {
        src_ptr = src_ptr->next;
        dst_ptr->next = new Node<T>(src_ptr->value);
        dst_ptr->next->prev = dst_ptr;
        dst_ptr = dst_ptr->next;
    }
}

template <typename T>
void LinkedList<T>::reset(LinkedList<T> &list)
{
    list._len = 0;
    list._head = nullptr;
}

template <typename T>
std::optional<typename LinkedList<T>::Node<T>> LinkedList<T>::find(std::size_t idx) const
{
    return std::nullopt;
}

template <typename T>
std::optional<std::size_t> LinkedList<T>::find(const T &item) const
{
    return std::nullopt;
}

template <typename T>
void LinkedList<T>::destroy(LinkedList<T> &list)
{
    if (_len == 0)
        return;

    Node<T> *tmp = _head;
    while (tmp)
    {
        _head = _head->next;
        delete tmp;
        tmp = _head;
    }
}

#endif // __LINKEDLIST_H__
