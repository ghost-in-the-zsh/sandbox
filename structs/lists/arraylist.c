#include "malloc.h"
#include "memory.h"
#include "assert.h"
#include "stdio.h"

#include "arraylist.h"

static const size_t DEFAULT_CAPACITY = 8;
static const size_t GROWTH_FACTOR = 2;

static void arraylist_extend(arraylist_t *src);

arraylist_t *arraylist_create_default(size_t item_bytes)
{
    return arraylist_create_with_capacity(item_bytes, DEFAULT_CAPACITY);
}

arraylist_t *arraylist_create_with_capacity(size_t sizeof_item, size_t capacity)
{
    assert(sizeof_item && capacity);

    arraylist_t *list = malloc(sizeof(arraylist_t));
    assert(list);

    list->items = malloc(capacity * sizeof_item);
    assert(list->items);

    list->bytes = sizeof_item;
    list->cap = capacity;
    list->len = 0;

    return list;
}

void arraylist_destroy(arraylist_t **list)
{
    assert(list && (*list)->items);
    free((*list)->items);
    free(*list);
    *list = NULL;
}

size_t arraylist_is_empty(arraylist_t *list)
{
    assert(list);
    return !list->len;
}

size_t arraylist_is_full(arraylist_t *list)
{
    assert(list && list->items);
    return !(list->cap - list->len);
}

size_t arraylist_length(arraylist_t *list)
{
    return list->len;
}

void arraylist_push_front(arraylist_t *list, void *item)
{
    assert(list && item);

    if (arraylist_is_full(list))
        arraylist_extend(list);

    // move items back a slot to make space in the front
    for (size_t i = list->len; i > 0; --i)
    {
        size_t curr_offset = i * list->bytes;
        size_t prev_offset = (i - 1) * list->bytes;

        size_t curr_addr = (size_t)list->items + curr_offset;
        size_t prev_addr = (size_t)list->items + prev_offset;

        memcpy((void *)curr_addr, (void *)prev_addr, list->bytes);
    }
    memcpy(list->items, item, list->bytes);
    ++list->len;
}

void arraylist_push_back(arraylist_t *list, void *item)
{
    assert(list && item);

    if (arraylist_is_full(list))
        arraylist_extend(list);

    size_t offset = list->len * list->bytes;
    size_t addr = (size_t)list->items + offset;
    memcpy((void *)addr, item, list->bytes);
    ++list->len;
}

void arraylist_pop_front(arraylist_t *list, void *dst)
{
    assert(list && !arraylist_is_empty(list));

    arraylist_get(list, 0, dst);

    // move items from the back to fill space at the front
    for (size_t i = 0; i < list->len - 1; ++i)
    {
        size_t next_offset = (i + 1) * list->bytes;
        size_t curr_offset = i * list->bytes;

        size_t next_addr = (size_t)list->items + next_offset;
        size_t curr_addr = (size_t)list->items + curr_offset;

        memcpy((void *)curr_addr, (void *)next_addr, list->bytes);
    }

    --list->len;
}

void arraylist_pop_back(arraylist_t *list, void *dst)
{
    assert(list && !arraylist_is_empty(list));

    arraylist_get(list, list->len - 1, dst);
    --list->len;
}

void arraylist_get(arraylist_t *list, size_t idx, void *dst)
{
    assert(list && !arraylist_is_empty(list) && idx >= 0 && idx < list->len);
    size_t item_offset = idx * list->bytes;
    size_t item_addr = (size_t)list->items + item_offset;
    memcpy(dst, (void *)item_addr, list->bytes);
}

size_t arraylist_capacity(arraylist_t *list)
{
    return list->cap;
}

size_t arraylist_sizeof_item(arraylist_t *list)
{
    return list->bytes;
}

static void arraylist_extend(arraylist_t *list)
{
    /* overflow? what overflow?! */
    size_t total_bytes = list->cap * list->bytes * GROWTH_FACTOR;
    void *newitems = malloc(total_bytes);
    assert(newitems);
    memcpy(newitems, list->items, list->len * list->bytes);
    free(list->items);
    list->items = newitems;
    list->cap *= GROWTH_FACTOR;
}
