#ifndef __ARRAYLIST_H__
#define __ARRAYLIST_H__

#include <cassert>
#include <cstddef>
#include <algorithm>
#include <optional>

#include "list.hpp"

static const std::size_t DEFAULT_CAPACITY = 16;
static const std::size_t GROWTH_RATE = 2;

template <typename T>
class ArrayList : virtual public List<T>
{
public:
    ArrayList();
    ArrayList(ArrayList<T> &&source);
    ArrayList(const ArrayList<T> &source);
    ArrayList(std::size_t capacity);
    virtual ~ArrayList();

    virtual void push_front(const T &item);
    virtual void push_back(const T &item);
    virtual std::optional<T> pop_front();
    virtual std::optional<T> pop_back();
    virtual std::optional<std::size_t> has(const T &item) const;
    virtual std::optional<T> get(std::size_t index) const;
    virtual std::size_t len() const;

    bool operator==(const ArrayList<T> &rhs) const;
    bool operator!=(const ArrayList<T> &rhs) const;

    ArrayList<T> &operator=(const ArrayList<T> &rhs);
    ArrayList<T> &operator=(ArrayList<T> &&rhs);

private:
    T *_items;
    std::size_t _len;
    std::size_t _capacity;

    void copy(const ArrayList<T> &src);
    void expand();
    void reset(ArrayList<T> &list);
};

template <typename T>
ArrayList<T>::ArrayList() : ArrayList(DEFAULT_CAPACITY)
{
}

template <typename T>
ArrayList<T>::ArrayList(std::size_t capacity) : _items(new T[capacity]),
                                                _len(0),
                                                _capacity(capacity)
{
}

template <typename T>
ArrayList<T>::ArrayList(const ArrayList<T> &source) : _items(nullptr),
                                                      _len(0),
                                                      _capacity(DEFAULT_CAPACITY)
{
    copy(source);
}

template <typename T>
ArrayList<T>::ArrayList(ArrayList<T> &&source) : _items(nullptr),
                                                 _len(0),
                                                 _capacity(DEFAULT_CAPACITY)
{
    _items = source._items;
    _capacity = source._capacity;
    _len = source._len;

    reset(source);
}

template <typename T>
ArrayList<T>::~ArrayList()
{
    if (_items)
        delete[] _items;
}

template <typename T>
void ArrayList<T>::push_front(const T &item)
{
    if (_len == _capacity)
        expand();

    // move items towards the back to make space
    // at the front
    for (size_t i = _len; i > 0; --i)
        _items[i] = _items[i - 1];

    _items[0] = item;
    ++_len;
}

template <typename T>
void ArrayList<T>::push_back(const T &item)
{
    if (_len == _capacity)
        expand();

    _items[_len++] = item;
}

template <typename T>
std::optional<T> ArrayList<T>::pop_front()
{
    if (_len == 0)
        return std::nullopt;

    std::optional<T> item = std::make_optional<T>(_items[0]);

    // move items towards the front to fill in
    // the space left by the popped item
    for (size_t i = 0; i < _len - 1; ++i)
        _items[i] = _items[i + 1];

    --_len;
    return item;
}

template <typename T>
std::optional<T> ArrayList<T>::pop_back()
{
    if (_len == 0)
        return std::nullopt;

    return std::make_optional<T>(_items[--_len]);
}

template <typename T>
std::optional<std::size_t> ArrayList<T>::has(const T &item) const
{
    auto result = std::find(_items, _items + _len, item);
    auto end = _items + _len;
    if (result != end)
        return std::make_optional<T>(result - _items);

    return std::nullopt;
}

template <typename T>
std::optional<T> ArrayList<T>::get(std::size_t index) const
{
    return index < _len ? std::optional<T>{_items[index]} : std::nullopt;
}

template <typename T>
std::size_t ArrayList<T>::len() const
{
    return _len;
}

template <typename T>
bool ArrayList<T>::operator==(const ArrayList<T> &rhs) const
{
    if (_items == rhs._items)
        return true;

    return std::equal(_items, _items + _len, rhs._items, rhs._items + rhs._len);
}

template <typename T>
bool ArrayList<T>::operator!=(const ArrayList<T> &rhs) const
{
    return !(*this == rhs);
}

template <typename T>
ArrayList<T> &ArrayList<T>::operator=(const ArrayList<T> &rhs)
{
    delete[] _items;
    copy(rhs);
    return *this;
}

template <typename T>
ArrayList<T> &ArrayList<T>::operator=(ArrayList<T> &&rhs)
{
    delete[] _items;
    copy(rhs);
    reset(rhs);
    return *this;
}

template <typename T>
void ArrayList<T>::copy(const ArrayList<T> &src)
{
    _items = new T[src._capacity];
    _capacity = src._capacity;
    _len = src._len;
    std::copy(src._items, src._items + src._len, _items);
}

template <typename T>
void ArrayList<T>::expand()
{
    _capacity *= GROWTH_RATE;
    T *items = new T[_capacity];
    std::copy(_items, _items + _len, items);
    delete[] _items;
    _items = items;
}

template <typename T>
void ArrayList<T>::reset(ArrayList<T> &list)
{
    list._len = 0;
    list._capacity = 0;
    list._items = nullptr;
}

#endif // __ARRAYLIST_H__
