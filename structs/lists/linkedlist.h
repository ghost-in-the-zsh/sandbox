#ifndef __LINKEDLIST_H__
#define __LINKEDLIST_H__

#include "stddef.h"
#include "unistd.h"

typedef struct linknode_t
{
    void *item;
    struct linknode_t *prev;
    struct linknode_t *next;
} linknode_t;

typedef struct linkedlist_t
{
    linknode_t *head;
    size_t len;   /* element count */
    size_t bytes; /* sizeof item */
} linkedlist_t;

linkedlist_t *linkedlist_create(size_t item_bytes);
void linkedlist_destroy(linkedlist_t **list);

void linkedlist_push_front(linkedlist_t *list, void *item);
void linkedlist_push_back(linkedlist_t *list, void *item);
void linkedlist_pop_front(linkedlist_t *list, void *dst);
void linkedlist_pop_back(linkedlist_t *list, void *dst);
void linkedlist_get(linkedlist_t *list, size_t idx, void *dst);
size_t linkedlist_is_empty(linkedlist_t *list);
size_t linkedlist_length(linkedlist_t *list);
size_t linkedlist_sizeof_item(linkedlist_t *list);

#endif /* __LINKEDLIST_H__ */
