#include "stdio.h"
#include "stdlib.h"
#include "assert.h"

#include "linkedlist.h"

void test_ctor()
{
    fprintf(stdout, "%s ...", __func__);

    linkedlist_t *list = linkedlist_create(sizeof(int));
    assert(linkedlist_length(list) == 0);
    linkedlist_destroy(&list);

    printf("ok\n");
}

void test_dtor()
{
    fprintf(stdout, "%s ...", __func__);

    linkedlist_t *list = linkedlist_create(sizeof(int));
    linkedlist_destroy(&list);
    assert(list == NULL);

    printf("ok\n");
}

void test_push_front()
{
    fprintf(stdout, "%s ...", __func__);

    const size_t MAX = 5;

    linkedlist_t *list = linkedlist_create(sizeof(size_t));
    for (size_t i = 0; i < MAX; ++i)
        linkedlist_push_front(list, &i);

    assert(linkedlist_length(list) == MAX);

    for (size_t i = 0; i < MAX; ++i)
    {
        size_t v;
        linkedlist_get(list, i, &v);
        assert(v == MAX - i - 1);
    }

    linkedlist_destroy(&list);

    printf("ok\n");
}

void test_push_back()
{
    fprintf(stdout, "%s ...", __func__);

    const size_t MAX = 5;

    linkedlist_t *list = linkedlist_create(sizeof(size_t));
    for (size_t i = 0; i < MAX; ++i)
        linkedlist_push_back(list, &i);

    assert(linkedlist_length(list) == MAX);

    for (size_t i = 0; i < MAX; ++i)
    {
        size_t v;
        linkedlist_get(list, i, &v);
        assert(v == i);
    }

    linkedlist_destroy(&list);

    printf("ok\n");
}

void test_pop_front()
{
    fprintf(stdout, "%s ...", __func__);

    const size_t MAX = 16;

    linkedlist_t *list = linkedlist_create(sizeof(size_t));
    for (size_t i = 0; i < MAX; ++i)
        linkedlist_push_back(list, &i);

    assert(linkedlist_length(list) == MAX);

    for (size_t i = 0; i < MAX; ++i)
    {
        size_t item;
        linkedlist_pop_front(list, &item);
        assert(item == i);
    }

    assert(linkedlist_length(list) == 0);
    linkedlist_destroy(&list);

    printf("ok\n");
}

void test_pop_back()
{
    fprintf(stdout, "%s ...", __func__);

    const size_t MAX = 16;

    linkedlist_t *list = linkedlist_create(sizeof(size_t));
    for (size_t i = 0; i < MAX; ++i)
        linkedlist_push_back(list, &i);

    assert(linkedlist_length(list) == MAX);

    // `int` to avoid underflow
    for (int i = MAX - 1; i >= 0; --i)
    {
        size_t item;
        linkedlist_pop_back(list, &item);
        assert(item == i);
    }

    assert(linkedlist_length(list) == 0);
    linkedlist_destroy(&list);

    printf("ok\n");
}

void test_get()
{
    fprintf(stdout, "%s ...", __func__);

    size_t i = 5;
    size_t v = 0;

    linkedlist_t *list = linkedlist_create(sizeof(size_t));
    linkedlist_push_back(list, &i);
    linkedlist_get(list, 0, &v);
    assert(i == v);

    printf("ok\n");
}

void test_length()
{
    fprintf(stdout, "%s ...", __func__);

    linkedlist_t *list = linkedlist_create(sizeof(size_t));
    assert(linkedlist_length(list) == 0);

    printf("ok\n");
}

void test_is_empty()
{
    fprintf(stdout, "%s ...", __func__);

    linkedlist_t *list = linkedlist_create(sizeof(size_t));
    assert(linkedlist_is_empty(list));

    printf("ok\n");
}

int main(int argc, char **argv)
{
    setbuf(stdout, NULL); /* disable line buffering */
    setbuf(stderr, NULL);

    test_ctor();
    test_dtor();
    test_push_front();
    test_push_back();
    test_pop_front();
    test_pop_back();
    test_get();
    test_length();
    test_is_empty();

    return EXIT_SUCCESS;
}
