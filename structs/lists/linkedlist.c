#include "malloc.h"
#include "memory.h"
#include "assert.h"
#include "stdio.h"

#include "linkedlist.h"

static linknode_t *linknode_create(void *item, size_t sizeof_item);
static void linknode_destroy(linknode_t *node);

linkedlist_t *linkedlist_create(size_t sizeof_item)
{
    assert(sizeof_item);

    linkedlist_t *list = malloc(sizeof(linkedlist_t));
    assert(list);

    memset(list, 0, sizeof(linkedlist_t));
    list->bytes = sizeof_item;
    return list;
}

void linkedlist_destroy(linkedlist_t **list)
{
    assert(*list);

    linknode_t *node = (*list)->head;
    while (node)
    {
        (*list)->head = (*list)->head->next;
        linknode_destroy(node);
        node = (*list)->head;
    }
    free(*list);
    *list = NULL;
}

size_t linkedlist_is_empty(linkedlist_t *list)
{
    assert(list);
    return !list->len;
}

size_t linkedlist_length(linkedlist_t *list)
{
    assert(list);
    return list->len;
}

void linkedlist_push_front(linkedlist_t *list, void *item)
{
    assert(list && item);

    linknode_t *front = linknode_create(item, list->bytes);
    front->next = list->head;
    list->head = front;

    ++list->len;
}

void linkedlist_push_back(linkedlist_t *list, void *item)
{
    assert(list && item);

    linknode_t *node = linknode_create(item, list->bytes);
    if (linkedlist_is_empty(list))
    {
        list->head = node;
    }
    else
    {
        linknode_t *traveler = list->head;
        while (traveler->next)
        {
            traveler = traveler->next;
        }
        traveler->next = node;
        node->prev = traveler;
    }

    ++list->len;
}

void linkedlist_pop_front(linkedlist_t *list, void *dst)
{
    assert(!linkedlist_is_empty(list) && dst);

    linknode_t *node = list->head;
    list->head = list->head->next;
    if (list->head)
        list->head->prev = NULL;
    memcpy(dst, node->item, list->bytes);
    linknode_destroy(node);

    --list->len;
}

void linkedlist_pop_back(linkedlist_t *list, void *dst)
{
    assert(!linkedlist_is_empty(list) && dst);

    linknode_t *node = list->head;
    while (node->next)
        node = node->next;

    // if `prev` points to a node, then just cut off the
    // link to this node; otherwise, we're removing the
    // last/only node and we must set `head` to null
    if (node->prev)
        node->prev->next = NULL;
    else
        list->head = NULL;

    memcpy(dst, node->item, list->bytes);
    linknode_destroy(node);

    --list->len;
}

void linkedlist_get(linkedlist_t *list, size_t idx, void *dst)
{
    assert(list && idx < linkedlist_length(list) && dst);

    linknode_t *node = list->head;
    for (size_t i = 0; i != idx; ++i)
        node = node->next;

    memcpy(dst, node->item, list->bytes);
}

size_t linkedlist_sizeof_item(linkedlist_t *list)
{
    return list->bytes;
}

static linknode_t *linknode_create(void *item, size_t sizeof_item)
{
    linknode_t *node = malloc(sizeof(linknode_t));
    assert(node);

    memset(node, 0, sizeof(linknode_t));
    node->item = malloc(sizeof_item);
    assert(node->item);

    memcpy(node->item, item, sizeof_item);
    assert(memcmp(node->item, item, sizeof_item) == 0);
    return node;
}

static void linknode_destroy(linknode_t *node)
{
    free(node->item);
    free(node);
}
