#include <cassert>
#include <algorithm>
#include <iostream>
using namespace std;

#include "arraylist.hpp"

void test_ctor_default()
{
    cout << __func__ << " ...";
    ArrayList<int> a;
    assert(a.len() == 0);
    cout << "ok" << endl;
}

void test_ctor_copy()
{
    cout << __func__ << " ...";
    ArrayList<int> a;
    ArrayList<int> b(a);
    assert(a.len() == b.len());
    cout << "ok" << endl;
}

void test_ctor_move()
{
    cout << __func__ << " ...";
    ArrayList<int> a;
    ArrayList<int> b(move(a));
    assert(a.len() == 0);
    assert(b.len() == 0);
    cout << "ok" << endl;
}

void test_length()
{
    cout << __func__ << " ...";
    ArrayList<int> a;
    assert(a.len() == 0);
    cout << "ok" << endl;
}

void test_push_front()
{
    cout << __func__ << " ...";
    const int MAX = 32;

    ArrayList<int> a;
    for (int i = 0; i < MAX; ++i)
        a.push_front(i);
    assert(a.len() == MAX);

    for (int i = 0; i < MAX; ++i)
        assert(i == a.pop_back());
    assert(a.len() == 0);

    cout << "ok" << endl;
}

void test_push_back()
{
    cout << __func__ << " ...";
    const int MAX = 32;
    ArrayList<int> a;

    for (int i = 0; i < MAX; ++i)
        a.push_back(i);
    assert(a.len() == MAX);

    for (int i = 0; i < MAX; ++i)
        assert(i == a.pop_front());
    assert(a.len() == 0);

    cout << "ok" << endl;
}

void test_pop_front()
{
    cout << __func__ << " ...";
    const int MAX = 32;

    ArrayList<int> a;
    for (int i = 0; i < MAX; ++i)
        a.push_front(i);
    assert(a.len() == MAX);

    for (int i = MAX - 1; i >= 0; --i)
        assert(i == a.pop_front().value());
    assert(a.len() == 0);

    assert(!a.pop_front().has_value());

    cout << "ok" << endl;
}

void test_pop_back()
{
    cout << __func__ << " ...";
    const int MAX = 32;

    ArrayList<int> a;
    for (int i = 0; i < MAX; ++i)
        a.push_back(i);
    assert(a.len() == MAX);

    for (int i = MAX - 1; i >= 0; --i)
        assert(i == a.pop_back().value());
    assert(a.len() == 0);

    assert(!a.pop_back().has_value());

    cout << "ok" << endl;
}

void test_has()
{
    cout << __func__ << " ...";

    ArrayList<int> a;
    a.push_back(2);
    a.push_back(4);
    a.push_back(6);

    assert(a.has(2).value() == 0);
    assert(!a.has(3).has_value());

    cout << "ok" << endl;
}

void test_get()
{
    cout << __func__ << " ...";

    ArrayList<int> a;
    a.push_back(2);
    a.push_back(4);

    optional<int> v1 = a.get(0);
    optional<int> v2 = a.get(10);

    assert(v1.has_value());
    assert(v1.value() == 2);

    assert(!v2.has_value());
    assert(v2.value_or(5) == 5);

    cout << "ok" << endl;
}

void test_equals()
{
    cout << __func__ << " ...";
    const int MAX = 8;

    ArrayList<int> a;
    for (int i = 0; i < MAX; ++i)
        a.push_back(i);

    ArrayList<int> b(a);

    assert(a == b);

    cout << "ok" << endl;
}

void test_not_equals()
{
    cout << __func__ << " ...";
    const int MAX = 8;

    ArrayList<int> a;
    for (int i = 0; i < MAX; ++i)
        a.push_back(i);

    ArrayList<int> b;
    for (int i = MAX - 1; i >= 0; --i)
        a.push_back(i);

    assert(a != b);

    cout << "ok" << endl;
}

void test_assign_copy()
{
    cout << __func__ << " ...";
    const size_t MAX = 64;

    ArrayList<size_t> a;
    for (size_t i = 0; i < MAX; ++i)
        a.push_back(i);

    ArrayList<size_t> b;
    b = a;
    assert(a == b);

    cout << "ok" << endl;
}

void test_assign_move()
{
    cout << __func__ << " ...";
    const size_t MAX = 8;

    ArrayList<size_t> a;
    for (size_t i = 0; i < MAX; ++i)
        a.push_back(i);

    ArrayList<size_t> b;
    b = move(a);

    for (int i = MAX - 1; i >= 0; --i)
        assert(i == b.pop_back().value());

    cout << "ok" << endl;
}

int main(int argc, char **argv)
{
    setbuf(stdout, NULL); /* disable line buffering */
    setbuf(stderr, NULL);

    test_ctor_default();
    test_ctor_copy();
    test_ctor_move();
    test_length();
    test_push_front();
    test_push_back();
    test_pop_front();
    test_pop_back();
    test_has();
    test_get();
    test_equals();
    test_not_equals();
    test_assign_copy();
    test_assign_move();

    return EXIT_SUCCESS;
}
