#include "stdio.h"
#include "stdlib.h"
#include "assert.h"

#include "arraylist.h"

void test_ctor_default()
{
    fprintf(stdout, "%s ...", __func__);

    arraylist_t *list = arraylist_create_default(sizeof(int));
    assert(arraylist_length(list) == 0);
    arraylist_destroy(&list);

    printf("ok\n");
}

void test_ctor_with_capacity()
{
    fprintf(stdout, "%s ...", __func__);
    const size_t CAP = 5;

    arraylist_t *list = arraylist_create_with_capacity(sizeof(int), CAP);
    assert(arraylist_length(list) == 0);
    assert(arraylist_is_empty(list));
    assert(arraylist_capacity(list) == CAP);
    arraylist_destroy(&list);

    printf("ok\n");
}

void test_dtor()
{
    fprintf(stdout, "%s ...", __func__);

    arraylist_t *list = arraylist_create_default(sizeof(int));
    arraylist_destroy(&list);
    assert(list == NULL);

    printf("ok\n");
}

void test_push_front()
{
    fprintf(stdout, "%s ...", __func__);

    const size_t MAX = 5;

    arraylist_t *list = arraylist_create_default(sizeof(size_t));
    for (size_t i = 0; i < MAX; ++i)
        arraylist_push_front(list, &i);

    assert(arraylist_length(list) == MAX);

    for (size_t i = 0; i < MAX; ++i)
    {
        size_t v;
        arraylist_get(list, i, &v);
        assert(v == MAX - i - 1);
    }

    arraylist_destroy(&list);

    printf("ok\n");
}

void test_push_back()
{
    fprintf(stdout, "%s ...", __func__);

    const size_t MAX = 5;

    arraylist_t *list = arraylist_create_default(sizeof(size_t));
    for (size_t i = 0; i < MAX; ++i)
        arraylist_push_back(list, &i);

    assert(arraylist_length(list) == MAX);

    for (size_t i = 0; i < MAX; ++i)
    {
        size_t v;
        arraylist_get(list, i, &v);
        assert(v == i);
    }

    arraylist_destroy(&list);

    printf("ok\n");
}

void test_pop_front()
{
    fprintf(stdout, "%s ...", __func__);

    const size_t MAX = 16;

    arraylist_t *list = arraylist_create_default(sizeof(size_t));
    for (size_t i = 0; i < MAX; ++i)
        arraylist_push_back(list, &i);

    assert(arraylist_length(list) == MAX);

    for (size_t i = 0; i < MAX; ++i)
    {
        size_t item;
        arraylist_pop_front(list, &item);
        assert(item == i);
    }

    assert(arraylist_length(list) == 0);
    arraylist_destroy(&list);

    printf("ok\n");
}

void test_pop_back()
{
    fprintf(stdout, "%s ...", __func__);

    const size_t MAX = 16;

    arraylist_t *list = arraylist_create_default(sizeof(size_t));
    for (size_t i = 0; i < MAX; ++i)
        arraylist_push_back(list, &i);

    assert(arraylist_length(list) == MAX);

    // `int` to avoid underflow
    for (int i = MAX - 1; i >= 0; --i)
    {
        size_t item;
        arraylist_pop_back(list, &item);
        assert(item == i);
    }

    assert(arraylist_length(list) == 0);
    arraylist_destroy(&list);

    printf("ok\n");
}

void test_get()
{
    fprintf(stdout, "%s ...", __func__);

    size_t i = 5;
    size_t v = 0;

    arraylist_t *list = arraylist_create_default(sizeof(size_t));
    arraylist_push_back(list, &i);
    arraylist_get(list, 0, &v);
    assert(i == v);

    printf("ok\n");
}

void test_length()
{
    fprintf(stdout, "%s ...", __func__);

    arraylist_t *list = arraylist_create_default(sizeof(size_t));
    assert(arraylist_length(list) == 0);

    printf("ok\n");
}

void test_is_empty()
{
    fprintf(stdout, "%s ...", __func__);

    arraylist_t *list = arraylist_create_default(sizeof(size_t));
    assert(arraylist_is_empty(list));

    printf("ok\n");
}

void test_is_full()
{
    fprintf(stdout, "%s ...", __func__);

    arraylist_t *list = arraylist_create_default(sizeof(size_t));
    assert(!arraylist_is_full(list));

    printf("ok\n");
}

int main(int argc, char **argv)
{
    setbuf(stdout, NULL); /* disable line buffering */
    setbuf(stderr, NULL);

    test_ctor_default();
    test_ctor_with_capacity();
    test_dtor();
    test_push_front();
    test_push_back();
    test_pop_front();
    test_pop_back();
    test_get();
    test_length();
    test_is_empty();
    test_is_full();

    return EXIT_SUCCESS;
}
