#ifndef __LIST_H__
#define __LIST_H__

#include <cstddef>
#include <optional>

template <typename T>
class List
{
public:
    virtual void push_front(const T &item) = 0;
    virtual void push_back(const T &item) = 0;
    virtual std::optional<T> pop_front() = 0;
    virtual std::optional<T> pop_back() = 0;
    virtual std::optional<std::size_t> has(const T &item) const = 0;
    virtual std::optional<T> get(std::size_t index) const = 0;
    virtual std::size_t len() const = 0;
};

#endif
