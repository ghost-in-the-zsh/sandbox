#ifndef __ARRAYLIST_H__
#define __ARRAYLIST_H__

#include "stddef.h"
#include "unistd.h"

typedef struct arraylist_t
{
    void *items;
    size_t len;   /* element count */
    size_t cap;   /* capacity */
    size_t bytes; /* sizeof item */
} arraylist_t;

arraylist_t *arraylist_create_default(size_t item_bytes);
arraylist_t *arraylist_create_with_capacity(size_t item_bytes, size_t capacity);
void arraylist_destroy(arraylist_t **list);

void arraylist_push_front(arraylist_t *list, void *item);
void arraylist_push_back(arraylist_t *list, void *item);
void arraylist_pop_front(arraylist_t *list, void *dst);
void arraylist_pop_back(arraylist_t *list, void *dst);
void arraylist_get(arraylist_t *list, size_t idx, void *dst);
size_t arraylist_is_empty(arraylist_t *list);
size_t arraylist_is_full(arraylist_t *list);
size_t arraylist_length(arraylist_t *list);
size_t arraylist_capacity(arraylist_t *list);
size_t arraylist_sizeof_item(arraylist_t *list);

#endif /* __ARRAYLIST_H__ */
