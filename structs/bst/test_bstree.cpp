#include <cassert>
#include <algorithm>
#include <iostream>
using namespace std;

#include "bstree.hpp"

void test_ctor_default()
{
    cout << __func__ << " ...";
    BSTree<int> a;
    assert(a.size() == 0);
    cout << "ok" << endl;
}

void test_ctor_copy()
{
    cout << __func__ << " ...";
    BSTree<int> a;
    BSTree<int> b(a);
    assert(a == b);
    cout << "ok" << endl;
}

void test_ctor_move()
{
    cout << __func__ << " ...";
    BSTree<int> a;
    BSTree<int> b(move(a));
    // assert(a.size() == 0);
    // assert(b.size() == 0);
    cout << "ok" << endl;
}

void test_traversal_postorder()
{
    cout << __func__ << " ...";
    cout << endl;
}

void test_insert()
{
    cout << __func__ << " ...";
    cout << endl;
}

void test_remove()
{
    cout << __func__ << " ...";
    cout << endl;
}

void test_traversal_preorder()
{
    cout << __func__ << " ...";
    cout << endl;
}

void test_traversal_inorder()
{
    cout << __func__ << " ...";
    cout << endl;
}

void test_has()
{
    cout << __func__ << " ...";
    cout << endl;
}

void test_get()
{
    cout << __func__ << " ...";
    cout << endl;
}

void test_assign_copy()
{
    cout << __func__ << " ...";
    cout << endl;
}

void test_assign_move()
{
    cout << __func__ << " ...";
    cout << endl;
}

int main(int argc, char **argv)
{
    setbuf(stdout, NULL); /* disable line buffering */
    setbuf(stderr, NULL);

    test_ctor_default();
    test_ctor_copy();
    // test_ctor_move();
    // test_insert();
    // test_remove();
    // test_traversal_preorder();
    // test_traversal_inorder();
    // test_traversal_postorder();
    // test_has();
    // test_assign_copy();
    // test_assign_move();

    return EXIT_SUCCESS;
}
