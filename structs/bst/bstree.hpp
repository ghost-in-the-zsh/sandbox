#ifndef __BST_H__
#define __BST_H__

#include <cassert>
#include <cstddef>
#include <algorithm>
#include <optional>
#include <queue>

template <typename T>
class BSTree
{
public:
    typedef T value_type;
    typedef T &ref_type;
    typedef const T &ref_const_type;
    typedef T *ptr_type;
    typedef T const *ptr_const_type;

    enum Traversal {
        PRE_ORDER,
        IN_ORDER,
        POST_ORDER,
    };

    BSTree();
    BSTree(BSTree<value_type> &&source);
    BSTree(const BSTree<value_type> &source);
    virtual ~BSTree();

    void insert(ref_const_type item);
    void remove(ref_const_type item);
    bool has(ref_const_type item) const;
    std::size_t size() const;

    std::queue<value_type> traverse(Traversal order) const;

    bool operator==(const BSTree<value_type> &rhs) const;
    bool operator!=(const BSTree<value_type> &rhs) const;

    BSTree<value_type> &operator=(const BSTree<value_type> &rhs);
    BSTree<value_type> &operator=(BSTree<value_type> &&rhs);

private:
    template <typename R>
    class Node
    {
    public:
        Node(const R &value) : value(value),
                               left(nullptr),
                               right(nullptr)
        {
        }
        R value;
        Node *left;
        Node *right;
    };

    Node<value_type> *_root;
    std::size_t _size;

    void preorder(std::queue<value_type> &q, Node<value_type> *root) const;
    void inorder(std::queue<value_type> &q, Node<value_type> *root) const;
    void postorder(std::queue<value_type> &q, Node<value_type> *root) const;
};

template <typename T>
BSTree<T>::BSTree() : _root(nullptr),
                      _size(0)
{
}

template <typename T>
BSTree<T>::BSTree(const BSTree<BSTree::value_type> &source) : _root(nullptr),
                                                              _size(0)
{
}

template <typename T>
BSTree<T>::BSTree(BSTree<BSTree::value_type> &&source) : _root(nullptr),
                                                         _size(0)
{
    _root = source._root;
    _size = source._size;
    // ...
}

template <typename T>
BSTree<T>::~BSTree()
{
    //
}

template <typename T>
void BSTree<T>::insert(BSTree::ref_const_type item)
{
    //
}

template <typename T>
void BSTree<T>::remove(BSTree::ref_const_type item)
{
    //
}

template <typename T>
bool BSTree<T>::has(BSTree::ref_const_type item) const
{
    // ...
    return false;
}

template <typename T>
std::size_t BSTree<T>::size() const
{
    return _size;
}

template <typename T>
std::queue<typename BSTree<T>::value_type> BSTree<T>::traverse(Traversal order) const
{
    std::queue<value_type> q;
    if (_size > 0) {
        switch (order) {
            case PRE_ORDER:
                ; // todo
            case IN_ORDER:
                inorder(q, _root);
            case POST_ORDER:
                ; // todo
        };
    }
    return q;
}

template <typename T>
bool BSTree<T>::operator==(const BSTree<BSTree::value_type> &rhs) const
{
    std::queue<value_type> lhsq = traverse(Traversal::IN_ORDER);
    std::queue<value_type> rhsq = rhs.traverse(Traversal::IN_ORDER);

    return lhsq == rhsq;
}

template <typename T>
bool BSTree<T>::operator!=(const BSTree<BSTree::value_type> &rhs) const
{
    return !(*this == rhs);
}

template <typename T>
BSTree<T> &BSTree<T>::operator=(const BSTree<BSTree::value_type> &rhs)
{
    return *this;
}

template <typename T>
BSTree<T> &BSTree<T>::operator=(BSTree<BSTree::value_type> &&rhs)
{
    // ...
    return *this;
}

template <typename T>
void BSTree<T>::preorder(
    std::queue<value_type> &q,
    Node<value_type> *root) const
{
    //
}

template <typename T>
void BSTree<T>::inorder(
    std::queue<value_type> &q,
    Node<value_type> *root) const
{
    if (root->left)
        inorder(q, root->left);

    q.push(root->value);

    if (root->right)
        inorder(q, root->right);
}

template <typename T>
void BSTree<T>::postorder(
    std::queue<value_type> &q,
    Node<value_type> *root) const
{
    //
}

#endif // __BST_H__
